#!/bin/sh

eval $(grep ^DATABASE_NAME= ../../strativ_assignment/.env);
eval $(grep ^DATABASE_USER= ../../strativ_assignment/.env);
eval $(grep ^DATABASE_PASSWORD= ../../strativ_assignment/.env);

echo ""Creating User.""

psql -c "CREATE USER $DATABASE_USER WITH PASSWORD '$DATABASE_PASSWORD'";

echo ""User created successfully.""

echo ""Setting client encoding.""

psql -c "ALTER ROLE $DATABASE_USER SET client_encoding TO 'utf8'";

echo ""Client encoding successfully set.""

echo ""Setting default transaction isolation.""

psql -c "ALTER ROLE $DATABASE_USER SET default_transaction_isolation TO 'read committed'";

echo ""Default transaction isolation successfully set.""

echo ""Setting timezone.""

psql -c "ALTER ROLE $DATABASE_USER SET timezone TO 'Asia/Dhaka'";

echo ""Time zone set successfully.""

echo ""Creating Database.""

psql -c "CREATE DATABASE $DATABASE_NAME WITH OWNER $DATABASE_USER;"

echo ""Database created successfully.""

echo ""Granting all permissions of the user to the database.""

psql -c "GRANT ALL PRIVILEGES ON DATABASE $DATABASE_NAME TO $DATABASE_USER;"

echo ""Permission successfully granted.""

echo ""Exiting the POSTGRES user.""
