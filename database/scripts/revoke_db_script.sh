#!/bin/sh

eval $(grep ^DATABASE_NAME= ../../strativ_assignment/.env);
eval $(grep ^DATABASE_USER= ../../strativ_assignment/.env);

echo ""Revoking all privileges on all tables for the user.""

psql -c "REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM $DATABASE_USER;"

echo ""All Tables privileges revoked.""

echo ""Revoking all privileges on all sequences for the user.""

psql -c "REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM $DATABASE_USER;"

echo ""All Sequences privileges revoked.""

echo ""Revoking all privileges on all functions for the user.""

psql -c "REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM $DATABASE_USER;"

echo ""All Functions privileges revoked.""

echo ""Revoking all privileges on all schemas for the user.""

psql -c "REVOKE ALL PRIVILEGES ON SCHEMA public FROM $DATABASE_USER;"

echo ""All Schema privileges revoked.""

echo ""Dropping user.""

psql -c "DROP USER $DATABASE_USER;"

echo ""User dropped.""

echo ""Dropping database.""

psql -c "DROP DATABASE $DATABASE_NAME;"

echo ""Database dropped.""
