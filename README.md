## STRATIV News

STRATIV News shows the fetched news headlines from NewsAPI service. The user doesn't have to create an account to use this
service but to get curated news the user must have to create an account.


## How to Run

STRATIV News needs Django 3.2, Python > 3, PostgreSQL > 12 and Redis to run.

1. First clone the repository to your local machine


                git clone git@bitbucket.org:saanpritom/strativ_assignment.git


2. Create a virtualenv in the project root directory and activate it. This project requires Python version above 3.0 to run
3. Install the dependencies

                pip install -r requirements.txt


4. Now go to the **strativ_assignment** directory and change the name of the file from **.env.example** to **.env**

5. Put your secret data on the ***.env*** file


                SECRET_KEY='your-secret-key'
                DATABASE_NAME='your-db-name'
                DATABASE_USER='your-db-user-name'
                DATABASE_PASSWORD='your-db-password'
                DATABASE_HOST='your-db-hostname'
                DATABASE_PORT='your-db-port'
                NEWSAPI_KEY='your-key-here'
                SENDGRID_API_KEY='your-key-here'
                ADMIN_EMAIL='your-email-address-here'


6. If you want to create your PostgreSQL database automatically then run the following commands. But you need to have **Bash** installed on your machine and make sure your user has the proper permissions for PostgreSQL.

                bash database/scripts/initialize_db.sh


    This will create a database on PostgreSQL with the provided values on your .env file.

7. If you want to delete the created database for some reason then run this script.

                bash database/scripts/revoke_db_script.sh


8. Now database is created it's time to run migrations. To run migrations please run this command.


                python manage.py makemigrations
                python manage.py migrate


    If you face any PostgreSQL related problem then most probably your PostgreSQL instance has some missing dependencies.

9. Now database is migrated at first you need to create a superuser for this application and to run Django Admin site to govern the application. To create a superuser use this command.

                python manage.py createsuperuser


    Now follow the terminal instructions to complete this process. After success a superuser will be created for the application.

10. Now it's time to run the application. To run it is development mode just use this command.

                python manage.py runserver


    A local server with your localhost url and default Django port 8000 will run. Now visit the application home page on any browser by http://<your-localhost>:8000 and you will see the home page.

11. To run this application into a production mode just do necessary changes on the settings.py file and run an extra command before running the server.

                python manage.py collectstatic



## How to setup notification scheduler

To setup scheduler you need Redis installed on your system and you also need Celery as an app dependency. But Celery supposed to be installed already if you have followed the above instructions properly.

1. Make sure all the dependencies are installed and configured.
2. Configure Celery broker URLs on the **settings.py** file.



                CELERY_BROKER_URL = 'redis://localhost:6379/0'
                CELERY_RESULT_BACKEND = 'redis://localhost:6379'
                CELERY_ACCEPT_CONTENT = ['application/json']
                CELERY_TASK_SERIALIZER = 'json'
                CELERY_RESULT_SERIALIZER = 'json'
                CELERY_TIMEZONE = 'Asia/Dhaka'


3. You may run Celery as a daemon process or can run on a terminal. To run this scheduler you need to run both Celery Worker and Celery Beat.
4. To run on a terminal please open a different terminal window and run this command


                /celery/installation/path/celery -A strativ_assignment worker --loglevel=INFO

    And on a different terminal window run this command


                /celery/installation/path/celery -A strativ_assignment beat -l INFO

    If everything is configured correctly then both the Celery Worker and Celery Beat is running for now and the Celery will queue the **scheduler tasks**.

5. The notification scheduler will run in every 15 minutes and notify the user via email if necessary.


## REST API Response
Only the curated news feed of an user responses can be parsed in a REST API format. To authenticate an user via API this application has used **DRF TokenAuthentication**

You need a token for an user to make the API request.

**How to Obtain Token**

You can see you token on the **Profile** page after your login. The **Token Key** is the key you needed to verify the user.

**Get API Response**

After getting you token you need to use any API client tool like cURL or Postman to make the response. Here is an example to the API request and response using cURL

    curl -X GET http://127.0.0.1:8000/api/v1/curated/news/ -H 'Authorization: Token 7b41b1a4648f2cc598c1496d2d29c4dfa3ea3b17'

Here you only need to change the URL and the Token to make a request.

1. The request and response is in JSON format.
2. If any errors occurred during this cycle the error responses will also is in JSON format
3. But any generic response like a 404 or 500 error will be in a HTML format and has a potential chance to stall your app. You must need to write proper response handler on your front end.


## Important Notes

This app has some limitations. The lists are given below.
1. No test cases have written but the functions and methods are tested manually due to short time.
2. You may face a **403 Forbidden Error**  when you are going to **RESET Password** if you run this application on your local machine and not on a cloud server.
3. You may face some problems on the news sorting forms and country related sorting.

These limitations exist due to a very short development time.


**This application is open sourced. You can use this application wherever you want to use and all the customization are allowed.**
