# Generated by Django 3.2 on 2021-04-15 01:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0004_delete_countrymodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usersettingsmodel',
            name='country_list',
            field=models.TextField(blank=True, null=True, verbose_name='Country List'),
        ),
        migrations.AlterField(
            model_name='usersettingsmodel',
            name='sources_list',
            field=models.TextField(blank=True, null=True, verbose_name='Sources List'),
        ),
    ]
