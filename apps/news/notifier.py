"""User keywords based email Notifier class."""
from django.core.mail import send_mail
from django.conf import settings
from .handlers import NewsFeedAPI
from .models import UserSettingsModel
from .scripts import convert_list_as_string


class EmailNotifier:
    """User email notification class."""

    model = UserSettingsModel
    news = NewsFeedAPI()

    def get_all_keywords_as_list(self, settings_obj):
        """Get all the keywords of a user."""
        keywords = settings_obj.keywords
        if keywords != '':
            return keywords.split(',')
        return None

    def get_news_headlines_by_sources_and_countries(self, settings_obj):
        """Get all the news headlines by user's country list."""
        response = self.news.get_top_headlines_by_sources_and_country(sources=settings_obj.sources_list,
                                                                      country=settings_obj.country_list)
        if 'status' not in response:
            return None
        return response

    def check_keyword_in_title(self, article, keyword):
        """check if the keyword is in the headline title."""
        if keyword in article['title']:
            return True
        return False

    def check_keyword_in_articles(self, article_list, keyword):
        """Check keyword in articles."""
        result = False
        for article in article_list:
            if self.check_keyword_in_title(article, keyword):
                result = True
                break
        return result

    def list_appeared_keywords(self, settings_obj):
        """List appeared keywords."""
        keyword_list = self.get_all_keywords_as_list(settings_obj)
        if keyword_list is None:
            return []
        news_response = self.get_news_headlines_by_sources_and_countries(settings_obj)
        if news_response is None:
            return []
        appeard_keyword_list = []
        for keyword in keyword_list:
            if self.check_keyword_in_articles(news_response['articles'], keyword):
                appeard_keyword_list.append(keyword)
        return appeard_keyword_list

    def email_user(self, settings_obj):
        """Email user if necessary."""
        keyword_list = self.list_appeared_keywords(settings_obj)
        if keyword_list:
            message = 'These keywords are appeared on the curated news feed. ' + convert_list_as_string(keyword_list)
            return send_mail('Keywords appeared on STRATIV News', message,
                             settings.ADMIN_EMAIL, [settings_obj.user.email], fail_silently=False,)
        return False

    def notify_user(self):
        """Notify a User if keyword appear."""
        settings_objects = self.model.objects.all()
        if settings_objects.count() > 0:
            for settings_object in settings_objects:
                return self.email_user(settings_object)
        return False
