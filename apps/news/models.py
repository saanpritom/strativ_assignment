from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class UserSettingsModel(models.Model):
    """User's settings data model."""

    country_list = models.TextField(null=True, blank=True, verbose_name='Country List')
    sources_list = models.TextField(null=True, blank=True, verbose_name='Sources List')
    keywords = models.TextField(null=True, blank=True, verbose_name='Keywords')
    user = models.OneToOneField(User, related_name="user_settings", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Represent default string."""
        return self.country_list

    class Meta:
        """Meta data for the model class."""

        verbose_name = 'User Setting'
        verbose_name_plural = 'User Settings'
        ordering = ['created_at']
