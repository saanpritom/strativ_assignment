from celery import shared_task
from .notifier import EmailNotifier


@shared_task(name="send_email_notification")
def send_notification():
    notifier = EmailNotifier()
    return notifier.notify_user()
