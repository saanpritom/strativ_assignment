"""News app internal urls."""
from django.urls import path
from .views import (HomeView, CuratedNewsView, UserSettingsView, CuratedNewsAPIView)


urlpatterns = [
    path('', HomeView.as_view(), name='home-page-view'),
    path('user/curated/news/', CuratedNewsView.as_view(), name='curated-news-view'),
    path('user/settings/', UserSettingsView.as_view(), name='user-settings-view'),
    path('api/v1/curated/news/', CuratedNewsAPIView.as_view(), name='curated-news-api-view'),
]
