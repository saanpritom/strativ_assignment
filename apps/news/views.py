"""News app view classes."""
from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import FormMixin
from django.contrib import messages
from apps.users.mixins import CustomLoginRequiredMixin
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .models import UserSettingsModel
from .forms import NewsSortingForm, UserSettingsForm
from .handlers import NewsFeedAPI
from .scripts import convert_list_as_string


# Create your views here.
class HomeView(FormMixin, TemplateView):
    """Home page view of the app."""
    template_name = "news/home-view.html"
    form_class = NewsSortingForm

    def construct_string_parameter(self, value=None):
        """Construct checkboxes parameters for the API."""
        if value is not None:
            return convert_list_as_string(value)
        return ''

    def get_context_data(self, **kwargs):
        """Insert api news data into context."""
        kwargs.update(news=NewsFeedAPI().get_top_headlines_by_sources_and_country(page=self.request.GET.get('page'),
                                                                                  sources=self.construct_string_parameter(self.request.GET.getlist('sources_list')),
                                                                                  country=self.construct_string_parameter(self.request.GET.getlist('country_list'))))
        return super(HomeView, self).get_context_data(**kwargs)


class CuratedNewsView(CustomLoginRequiredMixin, TemplateView):
    """Curated news view of the app."""
    template_name = "news/curated-news-view.html"
    model = UserSettingsModel

    def get_object(self):
        """Return the settings object."""
        return self.model.objects.get(user=self.request.user)

    def get_user_settings_data(self, obj, field_name):
        """Return saved settings data of the user."""
        return getattr(obj, field_name)

    def get_context_data(self, **kwargs):
        """Insert api news data into context."""
        if self.model.objects.filter(user=self.request.user).exists():
            obj = self.get_object()
            if obj.country_list == '' and obj.sources_list == '':
                kwargs.update(curated='Not Found')
            else:
                kwargs.update(news=NewsFeedAPI().get_top_headlines_by_sources_and_country(page=self.request.GET.get('page'),
                                                                                          sources=self.get_user_settings_data(obj, 'sources_list'),
                                                                                          country=self.get_user_settings_data(obj, 'country_list')))
                kwargs.update(curated='Found')
        else:
            kwargs.update(curated='Not Found')
        return super(CuratedNewsView, self).get_context_data(**kwargs)


class UserSettingsView(CustomLoginRequiredMixin, CreateView):
    """Settings page view of the app."""

    template_name = "news/settings-page-view.html"
    form_class = UserSettingsForm
    model = form_class.Meta.model

    def get_context_data(self, **kwargs):
        """Send saved settings data."""
        if self.model.objects.filter(user=self.request.user).exists():
            obj = self.model.objects.get(user=self.request.user)
            kwargs.update(found='found')
            kwargs.update(prev_country=obj.country_list)
            kwargs.update(prev_sources=obj.sources_list)
            kwargs.update(prev_keywords=obj.keywords)
        else:
            kwargs.update(found='not found')
        return super(UserSettingsView, self).get_context_data(**kwargs)

    def update_or_create(self, country_list, sources_list, keywords):
        """Create if not exists update if exists."""
        try:
            obj = self.model.objects.get(user=self.request.user)
            obj.country_list = country_list
            obj.sources_list = sources_list
            obj.keywords = keywords
            obj.save()
        except self.model.DoesNotExist:
            obj = self.model(country_list=country_list, sources_list=sources_list,
                             keywords=keywords, user=self.request.user)
            obj.save()
        except Exception as e:
            return e
        return 'done'

    def post(self, request, *args, **kwargs):
        """Custom post method to handle checkbox data."""
        form = self.form_class(request.POST)
        if form.is_valid():
            country_list = convert_list_as_string(form.cleaned_data['country_list'])
            sources_list = convert_list_as_string(form.cleaned_data['sources_list'])
            keywords = form.cleaned_data['keywords']
            result = self.update_or_create(country_list, sources_list, keywords)
            if result == 'done':
                messages.add_message(request, messages.SUCCESS, 'Settings updated successfully')
            else:
                messages.add_message(request, messages.ERROR, result)
        return render(request, self.template_name, {'form': self.form_class})


class CuratedNewsAPIView(GenericAPIView):
    """REST API Response for the curated news page."""
    model = UserSettingsModel
    permission_classes = [IsAuthenticated]

    def get_settings_data(self, obj, field_name):
        try:
            return getattr(obj.user_settings, field_name)
        except obj.RelatedObjectDoesNotExis:
            return ''

    def get_data(self, obj):
        return NewsFeedAPI().get_top_headlines_by_sources_and_country(sources=self.get_settings_data(obj, 'sources_list'),
                                                                      country=self.get_settings_data(obj, 'country_list'))

    def get(self, request, *args, **kwargs):
        data = self.get_data(self.request.user)
        if 'status' in data:
            return Response(data, status=status.HTTP_200_OK)
        return Response({'detail': data}, status=status.HTTP_424_FAILED_DEPENDENCY)
