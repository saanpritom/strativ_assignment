from django.contrib import admin
from .models import UserSettingsModel


# Register your models here.
class UserSettingsModelAdmin(admin.ModelAdmin):
    """Admin class for UserSettingModel."""

    list_display = ['id', 'country_list', 'sources_list', 'keywords', 'user']
    search_fields = ['keywords']


admin.site.register(UserSettingsModel, UserSettingsModelAdmin)
