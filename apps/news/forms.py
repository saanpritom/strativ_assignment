"""Forms is holding all the necessary forms for this app."""
from django import forms
from .models import UserSettingsModel
from .choices import country_list, sources_list


class NewsSortingForm(forms.Form):
    """Base news sorting form."""

    country_list = forms.ChoiceField(label='Country List', choices=country_list, widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-check-input', 'id': 'CountryList'}), required=False)
    sources_list = forms.ChoiceField(label='Sources List', choices=sources_list, widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-check-input', 'id': 'SourceList'}), required=False)


class UserSettingsForm(forms.ModelForm):
    """User settings page form."""

    country_list = forms.MultipleChoiceField(label='Country List', choices=country_list, widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-check-input', 'id': 'CountryList'}))
    sources_list = forms.MultipleChoiceField(label='Sources List', choices=sources_list, widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-check-input', 'id': 'SourceList'}))
    keywords = forms.CharField(label='Keywords', widget=forms.Textarea(attrs={'class': 'form-control', 'id': 'KeywordBox', 'row': '3'}), required=False)

    class Meta:
        """Meta data for the form class."""

        model = UserSettingsModel
        fields = ['country_list', 'sources_list', 'keywords']
