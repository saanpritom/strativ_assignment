"""Handlers classes or functions are responsible for interacting with external APIs."""
from django.conf import settings
from django.core.exceptions import ValidationError
from newsapi import NewsApiClient
from newsapi.newsapi_exception import NewsAPIException
from .scripts import convert_list_as_string, clean_page_number


class NewsFeedAPI:
    """This class will handle all the interaction between this app and the
    NewsFeed API Service."""

    def initialize_api_client(self):
        """Initialize the api service."""
        return NewsApiClient(api_key=settings.NEWSAPI_KEY)

    def get_newsapi_exception(self, exception):
        """Return news api exception message."""
        return exception.get_message()

    def get_generic_exception(self, exception):
        """Return generic exception message."""
        return str(exception)

    def check_news_response(self, news_response):
        """Check news response success."""
        if 'status' in news_response:
            if news_response['status'] == 'ok':
                return True
        return False

    def get_single_source_attribute(self, response_object, attr_name):
        """Extract attribute from the source response object."""
        res_list = []
        for data in response_object['sources']:
            if attr_name not in data:
                raise ValidationError('Attribute name not exits on source response object')
            res_list.append(data[attr_name])
        return res_list

    def get_customized_sources(self, response_object, single_attr=False, attr_name=None, as_string=False):
        """Customize the source response object."""
        if single_attr is True and attr_name is not None:
            response_object = self.get_single_source_attribute(response_object, attr_name)
        elif single_attr is True and attr_name is None:
            raise ValidationError('Attribute name can not be None if single_attr is True')
        if single_attr is True & as_string is True:
            response_object = convert_list_as_string(response_object)
        elif single_attr is False & as_string is True:
            raise ValidationError('as_string can not be True if only_id is False')
        return response_object

    def get_news_source_response(self, response, single_attr, attr_name, as_string):
        """Construct the news source response."""
        if single_attr is True or as_string is True:
            return self.get_customized_sources(response, single_attr, attr_name, as_string)
        else:
            return response

    def get_news_sources(self, single_attr=False, attr_name=None, as_string=False, country=None):
        """Return news sources."""
        try:
            response = self.newsapi.get_sources(country=country)
        except NewsAPIException as e:
            return self.get_newsapi_exception(e)
        except Exception as e:
            return self.get_generic_exception(e)
        return self.get_news_source_response(response, single_attr, attr_name, as_string)

    def get_all_articles(self, sources=None):
        """Return all articles from NewsFeedAPI."""
        if sources is None:
            sources = self.get_news_sources(True, 'id', True)
        try:
            response = self.newsapi.get_everything(sources=sources, language='en')
        except NewsAPIException as e:
            return self.get_newsapi_exception(e)
        except Exception as e:
            return self.get_generic_exception(e)
        return response

    def get_top_headlines_by_sources(self, page=1, sources=None):
        """Return top headlines by sources from NewsFeedAPI."""
        try:
            response = self.newsapi.get_top_headlines(sources=sources, page=clean_page_number(page))
        except NewsAPIException as e:
            return self.get_newsapi_exception(e)
        except Exception as e:
            return self.get_generic_exception(e)
        return response

    def get_top_headlines_by_country(self, page=1, country=None):
        """Return top headlines by country from NewsFeedAPI."""
        try:
            response = self.newsapi.get_top_headlines(country=country, page=clean_page_number(page))
        except NewsAPIException as e:
            return self.get_newsapi_exception(e)
        except Exception as e:
            return self.get_generic_exception(e)
        return response

    def combine_news(self, response_one, response_two):
        """Combine two news."""
        if self.check_news_response(response_one) and self.check_news_response(response_two):
            response_one['article'] = response_one['articles'] + response_two['articles']
            response_one['totalResults'] = str(int(response_one['totalResults']) + int(response_two['totalResults']))
            return response_one
        elif self.check_news_response(response_one) is True and self.check_news_response(response_two) is False:
            return response_one
        elif self.check_news_response(response_one) is False and self.check_news_response(response_two) is True:
            return response_two
        else:
            return response_one + ' and ' + response_two

    def get_top_headlines_by_sources_and_country(self, page=1, sources=None, country=None):
        """Combine news from sources fetched and country fetched."""
        sources_news = self.get_top_headlines_by_sources(page, sources)
        country_news = self.get_top_headlines_by_country(page, country)
        return self.combine_news(country_news, sources_news)

    def __init__(self):
        """Initialize news api at constructor."""
        self.newsapi = self.initialize_api_client()
