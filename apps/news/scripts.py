"""Utiliy functions for this app."""


def convert_list_as_string(sources_list):
    """Convert list elements into a comma separated string."""
    source_string = ''
    count = 0
    for data in sources_list:
        # for removing first comma
        if count == 0:
            source_string = data
        else:
            source_string = source_string + ',' + data
        count += 1
    return source_string


def clean_page_number(page_num):
    """Convert to Interger if not None."""
    if page_num is not None:
        return int(page_num)
    return 1
