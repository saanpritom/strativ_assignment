"""This template tags are used by NewsFeed Templates."""
from django import template
from ..scripts import convert_list_as_string
import math

register = template.Library()


@register.filter(name='check_attr')
def check_attr(value, arg):
    """Check if attribute exists in the value object."""
    if arg in value:
        return True
    return False


@register.filter(name='check_if_divisable')
def check_if_divisable(value, arg):
    """Check if the value is divisable."""
    if value % arg == 0:
        return True
    else:
        return False


@register.filter(name='check_attribute_exists')
def check_attribute_exists(obj_name, attr_name):
    """Check attribute existance."""
    if hasattr(obj_name, attr_name):
        return True
    return False


@register.filter(name='calculate_pagination')
def calculate_pagination(value, arg=1):
    """Calculate the pagination value."""
    return math.ceil(int(arg) / 20) + 1


@register.filter(name='current_page_number')
def current_page_number(value):
    """Return current page number."""
    current_page_num = value.GET.get('page')
    if current_page_num is None:
        return 1
    return current_page_num


@register.filter(name='current_source_parameters')
def current_source_parameters(value):
    """Return current page sources_list parameters."""
    sources_list = value.GET.getlist('sources_list')
    if sources_list is None or sources_list == '':
        sources_list = ''
    return convert_list_as_string(sources_list)


@register.filter(name='current_countries_parameters')
def current_countries_parameters(value):
    """Return current page countries_list parameters."""
    country_list = value.GET.getlist('country_list')
    if country_list is None or country_list == '':
        country_list = ''
    return convert_list_as_string(country_list)


@register.inclusion_tag('news/pagination-page-view.html')
def paginate_view(value, page_num, page_url, source_params, countries_params):
    """Create pagination page."""
    page_list = []
    for i in range(1, int(value)):
        page_list.append(i)
    return {'choices': page_list, 'curr_page': int(page_num), 'page_url': page_url,
            'source_params': source_params, 'countries_params': countries_params}
