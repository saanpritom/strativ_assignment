"""News app internal urls."""
from django.urls import path
from .views import (SignINView, LogOUTView, SignUPView, ProfileView, UserUpdateView,
                    UserPasswordUpdateView, UserPasswordUpdateDoneView,
                    UserPasswordResetView, UserPasswordResetDoneView)


urlpatterns = [
    path('signin/', SignINView.as_view(), name='login-view'),
    path('signout/', LogOUTView.as_view(), name='logout-view'),
    path('signup/', SignUPView.as_view(), name='signup'),
    path('profile/', ProfileView.as_view(), name='user-profile-view'),
    path('profile/update/', UserUpdateView.as_view(), name='user-profile-update'),
    path('password/update/', UserPasswordUpdateView.as_view(), name='user-password-update'),
    path('password/update/success/', UserPasswordUpdateDoneView.as_view(), name='password-update-done'),
    path('password/reset/', UserPasswordResetView.as_view(), name='password-reset-view'),
    path('password/reset/success/', UserPasswordResetDoneView.as_view(), name='password-reset-done-view'),
]
