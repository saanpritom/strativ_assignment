from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.contrib.auth.views import (LoginView, LogoutView,
                                       PasswordChangeView, PasswordChangeDoneView,
                                       PasswordResetView, PasswordResetDoneView)
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from .forms import UserInfoForm, UserUpdateForm
from .mixins import CustomLoginRequiredMixin


# Create your views here.
class SignINView(LoginView):
    """User signin view."""
    template_name = "registration/login_view.html"
    redirect_authenticated_user = True


class LogOUTView(LogoutView):
    """User Logout view."""
    template_name = "registration/logged_out_view.html"


class SignUPView(CreateView):
    """User registration view."""

    template_name = "registration/signup.html"
    form_class = UserInfoForm
    model = form_class.Meta.model
    success_url = reverse_lazy('user-profile-view')


class ProfileView(CustomLoginRequiredMixin, DetailView):
    """User profile view."""

    template_name = "user/profile-view.html"
    model = User
    context_object_name = 'data'

    def get_object(self):
        """Return object based on request user."""
        try:
            return self.model.objects.get(id=self.request.user.id)
        except Exception as e:
            raise ObjectDoesNotExist(e)


class UserUpdateView(CustomLoginRequiredMixin, UpdateView):
    """User profile update view."""

    template_name = "user/profile-update-view.html"
    form_class = UserUpdateForm
    model = form_class.Meta.model

    def get_object(self):
        """Return object based on request user."""
        try:
            return self.model.objects.get(id=self.request.user.id)
        except Exception as e:
            raise ObjectDoesNotExist(e)

    def get_success_url(self):
        """Custom success URL."""
        return str(reverse_lazy('user-profile-view')) + '?update=success'


class UserPasswordUpdateView(CustomLoginRequiredMixin, PasswordChangeView):
    """User password update view."""

    template_name = 'registration/password_update_form.html'
    success_url = reverse_lazy('password-update-done')


class UserPasswordUpdateDoneView(CustomLoginRequiredMixin, PasswordChangeDoneView):
    """User password update view."""

    template_name = 'registration/password_update_done.html'


class UserPasswordResetView(PasswordResetView):
    """User password reset view."""

    template_name = "registration/password_reset_view.html"
    success_url = reverse_lazy('password-reset-done-view')


class UserPasswordResetDoneView(PasswordResetDoneView):
    """User password reset done view."""

    template_name = "registration/password_reset_done_view.html"
