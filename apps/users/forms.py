"""Forms is holding all the necessary forms for this app."""
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class UserInfoForm(UserCreationForm):
    """Base user info form."""

    class Meta(UserCreationForm.Meta):
        """Meta data about the form class."""
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']


class UserUpdateForm(UserChangeForm):
    """User update form."""

    class Meta(UserChangeForm.Meta):
        """Meta data about the form class."""
        fields = ['first_name', 'last_name', 'email']
