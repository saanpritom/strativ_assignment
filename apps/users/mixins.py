"""Custom mixins for the app."""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


class CustomLoginRequiredMixin(LoginRequiredMixin):
    """Custom login required mixins for the app."""

    login_url = reverse_lazy('login-view')
