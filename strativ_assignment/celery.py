import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'strativ_assignment.settings')

app = Celery('strativ_assignment')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


app.conf.beat_schedule = {
    # Scheduler Name
    'send-email-notification': {
        # Task Name (Name Specified in Decorator)
        'task': 'send_email_notification',
        # Schedule
        'schedule': 10.0
    },
}
