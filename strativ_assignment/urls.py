"""strativ_assignment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import handler404, handler500
from rest_framework.authtoken import views
from .views import error_handler404, error_handler500

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.news.urls'), name='news_app'),
    path('accounts/', include('apps.users.urls'), name='users_app'),
    path('authentication/', include('django.contrib.auth.urls'), name='default_auth_urls'),
    path('api-token-auth/', views.obtain_auth_token),
]

handler404 = error_handler404
handler500 = error_handler500
