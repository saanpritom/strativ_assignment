"""Generic views of the project."""
from django.shortcuts import render


def error_handler404(request, exception, template_name="404.html"):
    """Return default 404 error page."""
    return render(request, template_name, status=404)


def error_handler500(request, *args, **argv):
    """Return default 500 error page."""
    return render(request, '404.html', status=500)
